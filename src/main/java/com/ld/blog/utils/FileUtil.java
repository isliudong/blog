package com.ld.blog.utils;

import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author liudong
 */
public class FileUtil {
    public static DataInputStream getDataInputStream(String path) {
        try {
            URL url = new URL("https://" + path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5 * 1000);
            return new DataInputStream(url.openStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
